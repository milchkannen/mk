import {Pipe, PipeTransform} from "angular2/core";
/*
 * New date format
 * German style
 * Usage:
 *   date | formatDate:[style[short|medium|long], locale]
 * Example:
 *   {{ date | formatDate:short}}
 *   formats to: 24. 01. 2016
 *   {{ date | formatDate:medium}}
 *   formats to: 24. Jan 2016
 *   {{ date | formatDate:long}}
 *   formats to: 24. Januar 2016
*/
@Pipe({name: "formatDate"})

export class FormatDatePipe implements PipeTransform {
    private locale: string;
    private style: string;
    private options: Object = {};
    transform( value: any, args: string[] ): any {
        this.style = args[0][0];
        this.locale = args[0][1];
        if ( this.locale === "" ) this.locale = "en-US";

        switch (this.style) {
            case "short":
                this.options = {
                    year: "numeric", month: "numeric", day: "numeric" // , hour: "2-digit", minute: "2-digit"
                };
            break;
            case "medium":
                this.options = {
                    year: "numeric", month: "short", day: "numeric" // , hour: "2-digit", minute: "2-digit"
                };
            break;
            case "long":
                this.options = {
                    year: "numeric", month: "long", day: "numeric" // , hour: "2-digit", minute: "2-digit"
                };
            break;
            case "yyyy":
                this.options = {
                    year: "numeric" // , month: "long", day: "numeric" // , hour: "2-digit", minute: "2-digit"
                };
            break;
            default:
                this.options = {
                    year: "numeric", month: "numeric", day: "numeric" // , hour: "2-digit", minute: "2-digit"
                };
      }
      return value.toLocaleDateString(this.locale, this.options);
  }
}
