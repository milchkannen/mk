import {bootstrap}    from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from 'angular2/http';
import {ROUTER_PROVIDERS} from 'angular2/router';

import {MainComponent} from './main.component'

import {IntroComponent} from './components/intro.component';
import {PortfolioComponent} from './components/portfolio.component';
import {ContactComponent} from './components/contact.component';

import {CopyrightComponent} from './components/copyright.component';

bootstrap(MainComponent);

bootstrap(IntroComponent);
bootstrap(PortfolioComponent, [ HTTP_PROVIDERS ]);
bootstrap(ContactComponent);

bootstrap(CopyrightComponent);
