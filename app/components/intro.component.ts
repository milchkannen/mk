import {Component, NgZone, OnInit} from 'angular2/core';
import {NgSwitch, NgSwitchWhen, NgSwitchDefault} from 'angular2/common';


@Component({
    selector: 'mk-intro',
    template: `
        <header style="background-image: linear-gradient(rgba(255, 255, 255, 0), rgba(255, 255, 255, 0)), url('content/intro/images/bg_0.jpg')">
            <div class="banner">
                <div class="left" flex="39">
                    <span [ngSwitch]="deviceSize">
                        <span *ngSwitchWhen="'small'">
                            <a hide-gt-sm href="http://milchkannen.ch">
                            <img class="logo-sm" src="content/logo/milchkannen-label.png">
                            </a>
                        </span>
                        <span *ngSwitchWhen="'large'">
                            <a hide show-gt-sm href="http://milchkannen.ch">
                            <img class="logo-lg" src="content/logo/milchkannen-stamp.png">
                            </a>
                        </span>
                    </span>
                </div>
                <div class="right" flex="61" hide show-gt-sm>
                </div>
            </div>

            <div class="welcome">
                <div class="left"  flex="39" hide show-gt-sm>
                </div>
                <div class="right" flex="61">
                    <h1 class="md-display-3">{{intro.title}}</h1>
                    <h2 class="intro-welcome">{{intro.welcome}}</h2>
                    <hr>
                    <h3 class="md-display-1">{{intro.subtitle}}</h3>
            		<br>
                </div>
            </div>
        </header>`,
    directives: [NgSwitch, NgSwitchWhen, NgSwitchDefault]
})



export class IntroComponent {
    public name: string = 'milchKannen';
    public logo: string = './content/logo/milchkannen-stamp.png';


    public width: number;
    public height: number;

    public intro: Object = {
        "title": "Aus Abfall etwas Gutes",
        "subtitle": "Finden Sie hier die andere Art von Recycling",
        "welcome": "Willkommen bei Milchkannen — einem Projekt von Kilchenmann",
        "image": {
            "path": "content/header/images",
            "prefix": "header_",
            "extension": "jpg",
            "quantity": 1
        }
    };



    public deviceSize: string = 'large';

    ngOnInit() {
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        if (this.width <= 600) {
            this.deviceSize = 'small';
        }
    }

    constructor(ngZone: NgZone) {
        window.onresize = (e) => {
            ngZone.run(() => {
                this.width = window.innerWidth;
                this.height = window.innerHeight;
                if (this.width <= 600) {
                    this.deviceSize = 'small';
                }
                else {
                    this.deviceSize = 'large';
                }
            });
        };
    }


    getRandomInt(min, max): void {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    public randomImage4Intro = './content/intro/images/bg_' + this.getRandomInt(0, 0) + '.jpg';

}
