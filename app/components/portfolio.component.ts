import {Component}  from 'angular2/core';



@Component({
    selector: 'mk-portfolio',
    template: `
        <div class="center">
            <md-card *ngFor="#object of portfolio.objects; #i = index">
                <img md-card-image src="{{ object.image }}" alt="">
                <md-card-content>
                  <md-card-title>{{ object.title }}</md-card-title>
                  <p>{{ object.description }}</p>
                </md-card-content>
            </md-card>
        </div>`
})



export class PortfolioComponent {

    public portfolio: Object = {
        'objects': [
            {
                'title': 'Bild N° 1',
                'date': '',
                'description': '',
                'image': '',
                'url': ''
            },
            {
                'title': 'Bild N° 2',
                'date': '',
                'description': '',
                'image': '',
                'url': ''
            },
            {
                'title': 'Bild N° 3',
                'date': '',
                'description': '',
                'image': '',
                'url': ''
            }
        ],
    }
}
