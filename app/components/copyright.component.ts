import {Component} from "angular2/core";
import {FormatDatePipe} from "../pipes/date.pipe";


@Component({
    selector: "mk-copyright",
    pipes: [FormatDatePipe],
    template: `
        <p class="credits">
            <a href="http://milchkannen.ch" title="Milchkannen | upCycling &amp; reDesign">milchkannen</a> &mdash; a <a href="http://lakto.org" title="LAKTO | Design &amp; more">LAKTO</a> project &copy; 2009-{{date | formatDate:['yyyy', '']}} by <a href="http://about.me/milchkannen" title="Andr&eacute; Kilchenmann">Kilchenmann</a>
        </p>
    `
})



export class CopyrightComponent {
    public date = new Date();
}
