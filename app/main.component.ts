import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

// import {FormatDatePipe} from './pipes/date.pipe';

@Component({
    selector: 'landing-page',
    template: `
        <mk-intro>Gutes aus Abfall</mk-intro>
        <mk-portfolio></mk-portfolio>
        <mk-contact></mk-contact>
        <mk-copyright></mk-copyright>
    `,
//    pipes: [FormatDatePipe]
})


export class MainComponent {

    public date = new Date();


/*
    openModal(): void {
        console.log('You clicked on the open modal button');
    }
*/
    onScroll(event) {
//        console.log('scroll event', event);
    }

}
